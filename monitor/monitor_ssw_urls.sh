#!/bin/sh
#
#
# copy ssh keys
######################################################################3

#!/bin/sh

ADMIN_EMAIL="itshelp@ssw.uga.edu"
RETRY_MINS=5
RETRY_SECS=$(( $RETRY_MINS * 60 ))
RETRY_COUNT=3
RC=0
JOB_NAME="monitor_ssw_urls"

#############################################################
# monitoring details - URL^DESC^OWNER_EMAIL
#############################################################
MONIT_DETAILS="
https://ssw.uga.edu^SSW_main_site^
https://sswconnect.uga.edu^SSW_Connect_site^
https://sswtechtalk.uga.edu^SSW_TechTalk_Site^
https://sswinventory.ssw.uga.edu^SSW_Inventory^
https://assistantships.ssw.uga.edu^SSW_Assistantships^
https://scholarships.ssw.uga.edu^SSW_Scholarships^
https://admissions.ssw.uga.edu^SSW_Admissions^
https://courseeval.ssw.uga.edu^SSW_CourseEval^
https://field.ssw.uga.edu^SSW_Field^
"


for LINE in $MONIT_DETAILS
do
   URL=$( echo "$LINE" | awk -F'^' '{print $1}' )
   URL_DESC=$( echo "$LINE" | awk -F'^' '{print $2}' )
   URL_OWNER_EMAIL=$( echo "$LINE" | awk -F'^' '{print $3}' )
   echo "checking URL: $URL [$URL_DESC] - email: $URL_OWNER_EMAIL"
   httpcode=$( curl -LkIs -o /dev/null -w "%{http_code}" $URL )
   curl_rc=$?
   if [ $curl_rc -ne 0 ] || [ $httpcode != "200" ]
   then
      x=1
      while true
      do
         echo "$URL is not available, trying again in $RETRY_MINS minutes"
         sleep $RETRY_SECS

         httpcode1=$( curl -LkIs -o /dev/null -w "%{http_code}" $URL )
         new_curl_rc=$?
         if [ $new_curl_rc -eq 0 ] && [ $httpcode1 == "200" ]
         then
            echo "$URL is now available, moving on..."
            break
         else
            echo "returned http code is $httpcode1, continuing to check..."
         fi
         if [ $x -ge $RETRY_COUNT ]
         then
            echo "URL is still not available but exceeded retry count of $RETRY_COUNT, exiting check"
            break
         fi

         x=$(( $x + 1 ))
      done
      if [ $new_curl_rc -ne 0 ]
      then
         RC=$(( $RC + 1 ))
         echo "Sending email to $URL_OWNER_EMAIL"
         mail_sub="URL monitor failed for [ $URL_DESC ]"
         mail_msg="
Hi,

The URL $URL ( $URL_DESC ) is not responding and may currently be down.
 
Please check.

-----
SSW Jenkins automation


*automated message sent from jenkins job [ ${JOB_NAME} ]
"
         echo "$mail_msg" | mailx -r "do_not_reply@uga.com" -s "$mail_sub" "$ADMIN_EMAIL,$URL_OWNER_EMAIL"
         if [ $? -eq 0 ]
         then
            echo "sent email to $ADMIN_EMAIL,$URL_OWNER_EMAIL"
         else 
            echo "failed to send email to $ADMIN_EMAIL,$URL_OWNER_EMAIL"
         fi
      fi
   else
      echo "$URL is available [$httpcode]"
   fi
done


echo "$RC bad URLs found"
exit $RC

