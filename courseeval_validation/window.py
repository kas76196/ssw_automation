from emailChecker import EmailChecker
import sys
from PyQt6 import QtCore, QtWidgets
from PyQt6.QtGui import QIcon, QPixmap
from PyQt6.QtWidgets import (
    QApplication,
    QComboBox,
    QFormLayout,
    QLabel,
    QLineEdit,
    QVBoxLayout,
    QWidget,
    QFileDialog,
)
from PyQt6.QtQml import QQmlApplicationEngine
from PyQt6.QtCore import QRect


class Window(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Course Eval Email Validation")
        self.setWindowIcon(QIcon("qt.png"))
        self.setGeometry(500, 200, 400, 300)
        outerLayout = QVBoxLayout()
        topLayout = QFormLayout()
        self.comparisonPath = None
        self.originalPath = None

        self.sourceFileButton = QtWidgets.QPushButton()
        self.sourceFileButton.setGeometry(QtCore.QRect(15, 15, 30, 30))
        self.sourceFileButton.setText("Select a .csv")
        self.sourceFileButton.setObjectName("sourceFileButton")

        self.sourceFileButton.clicked.connect(self.getSourceFile)
        self.sourceLabel = QLineEdit()
        topLayout.addRow(self.sourceFileButton, self.sourceLabel)

        self.originalFileButton = QtWidgets.QPushButton()
        self.originalFileButton.setGeometry(QtCore.QRect(15, 15, 30, 30))
        self.originalFileButton.setText("Select a .xlsx")
        self.originalFileButton.setObjectName("originalFileButton")

        self.originalFileButton.clicked.connect(self.getSourceFileB)
        self.originalLabel = QLineEdit()

        topLayout.addRow(self.originalFileButton, self.originalLabel)

        self.studentNameBox = QComboBox()
        self.courseIDBox = QComboBox()
        self.courseCallNumberBox = QComboBox()
        self.studentMyIDBox = QComboBox()
        self.courseNameBox = QComboBox()
        self.instructorNameBox = QComboBox()
        self.instructorMyIDBox = QComboBox()
        self.termBox = QComboBox()

        self.studentNameBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.studentNameBox.setCurrentIndex(1)
        self.courseIDBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.courseIDBox.setCurrentIndex(2)
        self.courseCallNumberBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.courseCallNumberBox.setCurrentIndex(3)
        self.studentMyIDBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.studentMyIDBox.setCurrentIndex(0)
        self.courseNameBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.courseNameBox.setCurrentIndex(4)
        self.instructorNameBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.instructorNameBox.setCurrentIndex(6)
        self.instructorMyIDBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.instructorMyIDBox.setCurrentIndex(5)
        self.termBox.addItems(["A", "B", "C", "D", "E", "F", "G", "H"])
        self.termBox.setCurrentIndex(7)

        topLayout.addRow("Student My ID", self.studentMyIDBox)
        topLayout.addRow("Student Name", self.studentNameBox)
        topLayout.addRow("Course ID", self.courseIDBox)
        topLayout.addRow("Course Call Number", self.courseCallNumberBox)
        topLayout.addRow("Course Name", self.courseNameBox)
        topLayout.addRow("Instructor My ID", self.instructorMyIDBox)
        topLayout.addRow("Instructor Name", self.instructorNameBox)
        topLayout.addRow("Term/Semester", self.termBox)

        outerLayout.addLayout(topLayout)

        self.emailChecker = QtWidgets.QPushButton()
        self.emailChecker.setGeometry(QtCore.QRect(15, 15, 30, 30))
        self.emailChecker.setText("Check Emails")
        self.emailChecker.setObjectName("EmailChecker")

        self.emailChecker.clicked.connect(self.runComparison)
        outerLayout.addWidget(self.emailChecker)

        self.setLayout(outerLayout)

    def getSourceFile(self):
        fname = QFileDialog.getOpenFileName(
            self, "Open file", "c:\\", "Comma Separated Spreadsheet (*.csv)"
        )
        self.sourceLabel.setText(fname[0])
        self.comparisonPath = str(fname[0])

    def getSourceFileB(self):
        fname = QFileDialog.getOpenFileName(
            self, "Open file", "c:\\", "Microsoft Excel Workbook (*.xlsx)"
        )
        self.originalLabel.setText(fname[0])
        self.originalPath = str(fname[0])

    def runComparison(self):
        print(str(self.studentNameBox.currentIndex()))
        EmailChecker(
            str(self.originalLabel.text()),
            str(self.sourceLabel.text()),
            self.studentNameBox.currentIndex(),
            self.courseIDBox.currentIndex(),
            self.courseCallNumberBox.currentIndex(),
            self.studentMyIDBox.currentIndex(),
            self.courseNameBox.currentIndex(),
            self.instructorNameBox.currentIndex(),
            self.instructorMyIDBox.currentIndex(),
            self.termBox.currentIndex(),
        )
