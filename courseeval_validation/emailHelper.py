from openpyxl.styles import Font


# Returns a well formatted workbook to use.
def newFormattedWorkbook(workbook):
    worksheet = workbook.active

    header = Font(
        name="Calibri",
        size=14,
        bold=True,
        italic=False,
        vertAlign=None,
        underline="none",
        strike=False,
        color="FF000000",
    )

    worksheet.cell(1, 1).value = "Student Name"
    worksheet["A1"].font = header
    worksheet.column_dimensions["A"].width = 20
    worksheet.cell(1, 2).value = "Course ID"
    worksheet["B1"].font = header
    worksheet.column_dimensions["B"].width = 12
    worksheet.cell(1, 3).value = "Course Call Number"
    worksheet["C1"].font = header
    worksheet.column_dimensions["C"].width = 20
    worksheet.cell(1, 4).value = "Student MyID"
    worksheet["D1"].font = header
    worksheet.column_dimensions["D"].width = 20
    worksheet.cell(1, 5).value = "Course Name/Title"
    worksheet["E1"].font = header
    worksheet.column_dimensions["E"].width = 30
    worksheet.cell(1, 6).value = "Instructor Name"
    worksheet["F1"].font = header
    worksheet.column_dimensions["F"].width = 20
    worksheet.cell(1, 7).value = "Instructor MyID"
    worksheet["G1"].font = header
    worksheet.column_dimensions["G"].width = 30
    worksheet.cell(1, 8).value = "Term/Semester"
    worksheet["H1"].font = header
    worksheet.column_dimensions["H"].width = 15

    return worksheet

def newResultWorkbook(workbook, row):
    worksheet = workbook.active

    header = Font(
        name="Calibri",
        size=14,
        bold=True,
        italic=False,
        vertAlign=None,
        underline="none",
        strike=False,
        color="FF000000",
    )

    worksheet.cell(1, 1).value = "Instructor Name/ID"
    worksheet["A1"].font = header
    worksheet.cell(1, 2).value = "Course ID"
    worksheet["B1"].font = header
    worksheet.cell(1, 3).value = "Result"
    worksheet["C1"].font = header
    worksheet.cell(1, 4).value = "Replaced With"
    worksheet["D1"].font = header

    worksheet.cell(row, 1).value = "Student Name"
    worksheet.cell(row, 1).font = header
    worksheet.column_dimensions["A"].width = 25
    worksheet.cell(row, 2).value = "Course ID"
    worksheet.cell(row, 2).font = header
    worksheet.column_dimensions["B"].width = 12
    worksheet.cell(row, 3).value = "Course Call Number"
    worksheet.cell(row, 3).font = header
    worksheet.column_dimensions["C"].width = 25
    worksheet.cell(row, 4).value = "Student MyID"
    worksheet.cell(row, 4).font = header
    worksheet.column_dimensions["D"].width = 20
    worksheet.cell(row, 5).value = "Course Name/Title"
    worksheet.cell(row, 5).font = header
    worksheet.column_dimensions["E"].width = 30
    worksheet.cell(row, 6).value = "Instructor Name"
    worksheet.cell(row, 6).font = header
    worksheet.column_dimensions["F"].width = 20
    worksheet.cell(row, 7).value = "Instructor MyID"
    worksheet.cell(row, 7).font = header
    worksheet.column_dimensions["G"].width = 30
    worksheet.cell(row, 8).value = "Term/Semester"
    worksheet.cell(row, 8).font = header
    worksheet.column_dimensions["H"].width = 15

    return worksheet
