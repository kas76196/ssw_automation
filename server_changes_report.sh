#!/bin/sh
#
# this script simply reports any new/modified files
# on the server
#####################################################
VERSION="1.0.1"
echo "$VERSION"

mail_params="$1"
if [ -z "$mail_params" ]
then
   MAIL_RECIPIENTS="tpayne@uga.edu,rrysiew@uga.edu"
else
   MAIL_RECIPIENTS="$mail_params"
fi
DAYS=1
REPORT_DIR="/root/scripts/reports"
mkdir -p $REPORT_DIR
REPORT_DATE=$( date +'%m%d' )
REPORT=$( find /var/www /etc /var/tmp /var/lib/tomcat8 -mtime -$DAYS -type f -not -path "*logs/*" -ls -exec du -sm {} \; )

if [ ! -z "$REPORT" ]
then
        MSG="Script: $0\nReport Directory: $REPORT_DIR\n\n\n"
        echo "$REPORT" > ${REPORT_DIR}/${REPORT_DATE}.txt
        if [ $( egrep -i "[a-z]" ${REPORT_DIR}/${REPORT_DATE}.txt | wc -l ) -gt 0 ]
        then
           echo sending email to $MAIL_RECIPIENTS
           cat ${REPORT_DIR}/${REPORT_DATE}.txt | mailx -s "Server changes report for the last $DAYS day(s) on $(hostname)" "$MAIL_RECIPIENTS"
        else
           echo "There are no changes to report."
        fi
fi
