#!/bin/sh
#
# uninstall tomcat
# execute with sudo
#################################################

service tomcat8 stop
service tomcat8-admin stop
echo remove apache conf files
rm -f /etc/apache2/sites-enabled/assistantships-ssl.conf /etc/apache2/sites-enabled/assistantships.conf
rm -f /etc/apache2/sites-enabled/courseeval-ssl.conf /etc/apache2/sites-enabled/courseeval.conf
dpkg -l|egrep "tomcat|mod-jk"
echo "removing packages"
apt-get purge -y tomcat8 tomcat8-docs tomcat8-examples tomcat8-admin tomcat8-common libapache2-mod-jk
echo "complete purge of package list"
dpkg -l|grep tomcat
echo "executing autoremove"
apt-get autoremove --purge -y
echo "removing directories"
test -d "/etc/tomcat8" && rm -rf /etc/tomcat8
test -d "/var/lib/tomcat8" && rm -rf /var/lib/tomcat8
test -d "/usr/share/tomcat8" && rm -rf /usr/share/tomcat8
echo "listing packages"
pkg_count=$( dpkg -l | egrep "tomcat|mod-jk" | wc -l )
test $pkg_count -gt 0 && exit 150
echo "exiting uninstall script"
exit