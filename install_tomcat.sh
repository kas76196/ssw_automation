#!/bin/sh
#
# install tomcat
# execute with sudo
#################################################
G_RC=0
modules_list="tomcat8 tomcat8-docs tomcat8-examples tomcat8-admin libapache2-mod-jk"
echo install tomcat;
apt-get install -y tomcat8 tomcat8-docs tomcat8-examples tomcat8-admin libapache2-mod-jk
for mod in $modules_list
do
   dpkg -l|egrep "$mod"
   rc=$?
   G_RC=$(( $G_RC + $rc ))
   if [ $rc -ne 0 ] 
   then
      echo " - ERROR: $mod failed to install"
   fi
done

if [ $G_RC -ne 0 ]
then
   echo "ERROR: package(s) failed to install"
   exit 100
fi

dpkg -l|egrep "tomcat|mod-jk"
service tomcat8 start
#service tomcat8-admin start
sleep 5
java_proc_count=$( ps -ef | grep java | grep -v grep | wc -l )
if [ $java_proc_count -lt 1 ]
then
   echo "ERROR: failed to start tomcat, please verify the installs."
   exit 200
else 
   echo "SUCCESS!"
fi

exit