#!/bin/sh
#
#
# copy ssh keys
######################################################################3

REMOTE_HOST=$1

if [ -z "$REMOTE_HOST" ]
then
   echo "Please enter a remote host ip address; the host must have user svcjenkins"
   exit 10
fi

cd $HOME/.ssh
ssh-copy-id -i id_rsa.pub svcjenkins@$REMOTE_HOST -p 22390
