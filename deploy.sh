#!/bin/sh
#
#
# deploy code from bitbucket repositories
#####################################################
VERSION="1.0.62"
USAGE="
$0 <repository name> <environment (dev/qa/prod)>
i.e. $0 'ssw_automation' 'dev'
"

REPO_NAME=$1
APP_ENV=$2
BRANCH=""
APP_SERVER=""
ENV_CONF="env-conf"
REMOTE_USER="svcjenkins"
SSW_PROJECTS_DIR="/ssw_projects/git_repos"

APP_CONFIG_REPO_DIR="$SSW_PROJECTS_DIR/${ENV_CONF}_master"
AUTOMATION_REPO_DIR="$SSW_PROJECTS_DIR/ssw_automation_master"

APPENV_CONFIG="$APP_CONFIG_REPO_DIR/appenv.conf"
APP_CONFIG_FILE="appConfig.php"
CONFIG_FOLDER=""
APACHE_VER="apache2"
APACHE_VHOST_CONF_DIR="/etc/apache2/sites-available"
APACHE_CERT_DIR="/etc/apache2/ssl"
SSH_PORT="22390"
PHP_VER="php5.6-mysql"
JAVA_PACKAGES_DIR="$AUTOMATION_REPO_DIR/java_packages"
JAVA_CONFIG_DIR="$APP_CONFIG_REPO_DIR/$APP_ENV/java_config"


echo "SCRIPT VERSION for $0: $VERSION"

if $( echo "$APP_ENV" | egrep -i "stage|qa" >/dev/null 2>&1 )
then
   BRANCH="master"
   APP_SERVER="172.17.51.80"
   APP_CONFIG_ENVDIR="qa"
   URL_ENV="qa"
elif $( echo "$APP_ENV" | egrep -i "prod" >/dev/null 2>&1 )
then
    BRANCH="master"
    APP_SERVER="172.17.51.78"
    APP_CONFIG_ENVDIR="prod"
    URL_ENV=""
elif $( echo "$APP_ENV" | egrep -i "dev" >/dev/null 2>&1 )
then
    BRANCH="develop"
    APP_SERVER="172.17.51.74"
    APP_CONFIG_ENVDIR="dev"
    URL_ENV="dev"
else
   echo "ERROR: undefined APP environment specified [$APP_ENV]"
   echo "$USAGE" 
   exit 19
fi

LOCAL_DIR_NAME="$SSW_PROJECTS_DIR/${REPO_NAME}_${BRANCH}"
CODE_DIR=$LOCAL_DIR_NAME

if [ -z "$REPO_NAME" ]
then
   echo "Please enter a bitbucket repository name"
   echo "$USAGE"
   exit 21
fi


if [ ! -d "$SSW_PROJECTS_DIR" ]
then
   echo "ERROR: unable to find the ssw projects directory: $SSW_PROJECTS_DIR"
   exit 22
fi

if [ -z "$BRANCH" ]
then
   echo "Please enter a bitbucket repository branch name"
   echo "$USAGE"
   exit 23 
fi


##################
# MAIN
##################
cd $SSW_PROJECTS_DIR
if [ $? -ne 0 ]
then
   echo "ERROR: unable to change directory to $SSW_PROJECTS_DIR"
   exit 101
fi

echo "Get latest code to $APP_CONFIG_REPO_DIR"
$AUTOMATION_REPO_DIR/getCode.sh "$ENV_CONF" 'master'
if [ $? -ne 0 ]
then
   echo "ERROR: unable to get latest code from $ENV_CONF repo"
   exit 102
fi

echo "Get latest code to $LOCAL_DIR_NAME"
$AUTOMATION_REPO_DIR/getCode.sh "$REPO_NAME" "$BRANCH"
if [ $? -ne 0 ]
then
   echo "ERROR: unable to get latest code from $REPO_NAME repo, $BRANCH branch"
   exit 103
fi

if [ -f "$APPENV_CONFIG" ]
then
   APP_CONFIG_NAME=$( cat $APPENV_CONFIG | grep -v '^#' | grep "\^$REPO_NAME\^" | tail -1 | awk -F'^' '{print $1}' )
   APP_CONFIG_CODEDIR=$( cat $APPENV_CONFIG | grep -v '^#' | grep "\^$REPO_NAME\^" | tail -1 | awk -F'^' '{print $3}' )
   if [ -z "$APP_CONFIG_CODEDIR" ]
   then
      echo "ERROR: unable to get Code directory from $APPENV_CONFIG"
      exit 104
   fi
   APP_APACHE_CONF_SSL=""
   APP_APACHE_CONF=$( cat $APPENV_CONFIG | grep -v '^#' | grep "\^$REPO_NAME\^" | tail -1 | awk -F'^' '{print $4}' )
   test ! -z "$APP_APACHE_CONF" && APP_APACHE_CONF_SSL=$( echo "$APP_APACHE_CONF" | sed 's/.conf//' )-ssl.conf
   APP_APACHE_CONF_BASE=$( echo "$APP_APACHE_CONF" | sed 's/.conf//' )
   APP_CONF_VAR=$( cat $APPENV_CONFIG | grep -v '^#' | grep "\^$REPO_NAME\^" | tail -1 | awk -F'^' '{print $5}' )
   APP_URL=$( cat $APPENV_CONFIG | grep -v '^#' | grep "\^$REPO_NAME\^" | tail -1 | awk -F'^' '{print $6}' | sed "s/<ENV>/$URL_ENV/g")
   APP_CONFIG_CODETYPE=$( cat $APPENV_CONFIG | grep -v '^#' | grep "\^$REPO_NAME\^" | tail -1 | awk -F'^' '{print $7}' )
   if [ -z "$APP_CONFIG_CODETYPE" ]
   then
      echo "ERROR: unable to get Code type from $APPENV_CONFIG"
      exit 105
   fi

   if [ -f "$APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/$APP_APACHE_CONF_SSL" ]
   then
      APP_SSL_EXISTS="yes"
   else
      APP_SSL_EXISTS="no"
   fi
   if $( basename "$APP_CONF_VAR" | grep "config" >/dev/null 2>&1 )
   then 
      CONFIG_FOLDER="config"
   else 
      CONFIG_FOLDER=$( echo "$APP_CONF_VAR" | sed "s#$APP_CONFIG_CODEDIR#.#" )
   fi
else
   echo "ERROR: unable to find app config file: $APPENV_CONFIG"
   exit 106
fi

echo "change directory to $LOCAL_DIR_NAME"
cd $LOCAL_DIR_NAME
if [ $? -ne 0 ]
then
   echo "ERROR: unable to change directory to $LOCAL_DIR_NAME"
   exit 111
fi

if $( echo "$APP_CONFIG_CODETYPE" | grep -i java >/dev/null 2>&1 )
then
   echo "copying settings.xml(contains uga proxy server details) to $HOME/.m2/."
   cp $JAVA_PACKAGES_DIR/settings.xml $HOME/.m2/.
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to move $JAVA_PACKAGES_DIR/settings.xml to $HOME/.m2/."
      exit 119
   fi

   echo "copying context.xml src/main/webapp/META-INF/"
   cp $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/context.xml src/main/webapp/META-INF/.
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to move context.xml from $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME to $(pwd)/src/main/webapp/META-INF/."
      exit 120
   fi

   echo "copying web.xml to src/main/webapp/WEB-INF/"
   cp $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/web.xml src/main/webapp/WEB-INF/.
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to move context.xml from $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME to $(pwd)/src/main/webapp/META-INF/."
      exit 120
   fi

   echo "compiling java code"
   mvn clean package
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to compile java code for $REPO_NAME"
      exit 121
   fi

   if [ ! -f "target/${REPO_NAME}.war" ]
   then
      echo "ERROR: unable to find generated .war file in $(pwd)/target"
      exit 122
   fi

   cp $JAVA_CONFIG_DIR/* $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/.
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to copy java config files from $JAVA_CONFIG_DIR to $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/."
      exit 123
   fi

   cp $JAVA_PACKAGES_DIR/* $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/.
   if [ $? -ne 0 ]
   then
      echo "ERROR: unable to copy java package files from $JAVA_PACKAGES_DIR to $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/."
      exit 124
   fi

   codetype_cmds="
   #sudo service tomcat8 stop;
   #sudo apt-get update -y; sudo apt-get upgrade -y;
   #echo starting tomcat server;
   #sudo systemctl start tomcat8;echo \$?;
"
   postcopy_cmds="
   sudo usermod -a -G tomcat8 svcjenkins;
   sudo mv $APP_CONFIG_CODEDIR/config/server.xml /var/lib/tomcat8/conf/.;
   sudo mv $APP_CONFIG_CODEDIR/config/workers.properties /etc/apache2/.;
   sudo mkdir -p $APP_CONFIG_CODEDIR/web;
   sudo perl -p -i -e \"s/JkWorkersFile \/etc\/libapache2-mod-jk\/workers.properties/JkWorkersFile \/etc\/apache2\/workers.properties/\"  /etc/apache2/mods-available/jk.conf;
   sudo perl -p -i -e \"s/^#JAVA_HOME=\/usr\/lib\/jvm\/java-7-openjdk/JAVA_HOME=\/usr\/lib\/jvm\/default-java/\"  /etc/default/tomcat8;
   sudo chmod -R +x /etc/tomcat8/;
   sudo chown -R tomcat8 /etc/tomcat8/;
   sudo chmod -R 755 /var/lib/tomcat8;
   sudo chmod 755 /var/log/tomcat8;
   sudo chmod 755 /var/cache/tomcat8;
"
elif $( echo "$APP_CONFIG_CODETYPE" | grep -i php >/dev/null 2>&1 )
then
   echo "Determined code type to be running on php"
   cd $LOCAL_DIR_NAME
   echo "CURRENT DIRECTORY:"
   pwd
   echo
   echo "create config directory if necessary"
   mkdir -p config
   if [ -d "$APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME" ]
   then
      cp -R $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/* $CONFIG_FOLDER
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to copy contents from env conf directory [ $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME ] into config/."
         exit 400
      fi
      cp -R $APP_CONFIG_REPO_DIR/SSW_CERT/* $CONFIG_FOLDER
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to copy ssw cert contents from env conf directory [ $APP_CONFIG_REPO_DIR/SSW_CERT ] into config/."
         exit 401
      fi
      
      if $( echo "$APP_CONFIG_NAME" | grep "sswinventory" >/dev/null 2>&1 )
      then
         cp $APPENV_CONFIG $CONFIG_FOLDER/.
      fi
   else 
      echo "ERROR: unable to find env conf directory [$APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME]"
      exit 405
   fi
   if [ -f "composer.json" ]
   then
      echo "executing php composer.phar install on $(hostname)"
      sudo composer self-update
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to execute composer self-update on $(hostname)"
         exit 431
      fi
      sudo composer install --prefer-dist
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to execute composer install --prefer-dist on $(hostname)"
         exit 432
      fi
      sudo php /var/lib/jenkins/composer-setup.php
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to execute composer-setup.php on $(hostname)"
         exit 433
      fi
      sudo php /usr/local/bin/composer install --no-interaction --ansi
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to download and setup all dependencies on $(hostname)"
         exit 410
      fi
      sudo php /usr/local/bin/composer update
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to execute composer update on $(hostname)"
         exit 411
      fi
   fi
   codetype_cmds="
sudo apt-get -y update;
echo install apache2;
sudo apt-get -y install apache2 libapache2-mod-php5.6 php5.6-fpm php5.6-cgi;
echo add-apt-repository;
sudo add-apt-repository -y ppa:ondrej/php;
conf_exists="no";
if [ ! -z \"${APP_APACHE_CONF}\" ] || [ ! -z \"${APP_APACHE_CONF_SSL}\" ];then
   conf_exists="yes";
   echo get php and associated modules;
   sudo apt-get -y install php5.6 php5.6-mysql php-gettext php5.6-mbstring php-mbstring php-xdebug php5.6-xml php5.6-ldap;
   sudo apt-get -y install curl php-cli php-mbstring git unzip;
   sudo a2dismod php7.0; 
   sudo a2dismod php8.0;
   echo downgrade php to version 5.6;
   sudo a2enmod php5.6; 
   sudo update-alternatives --set php /usr/bin/php5.6;
   sudo phpenmod pdo_mysql;
   sudo apt-get install -y php5.6-curl;
   sudo apt-get install -y php5.6-dom;
   echo updating php.ini to enable php_curl and php_openssl;
   set -x;
   sudo perl -p -i -e \"s/^\;extension=php_curl.dll/extension=php_curl.dll/\" /etc/php/5.6/apache2/php.ini;
   sudo perl -p -i -e \"s/^\;extension=php_ldap.dll/extension=php_ldap.dll/\" /etc/php/5.6/apache2/php.ini;
   sudo perl -p -i -e \"s/^\;extension=php_openssl.dll/extension=php_openssl.dll/\" /etc/php/5.6/apache2/php.ini;
   echo completed update of php.ini;
fi
"

   
   postcopy_cmds="
sudo chown -R root $APP_CONFIG_CODEDIR;
sudo chown -R root $APP_CONF_VAR;
sudo chmod -R 755 $APP_CONFIG_CODEDIR;
sudo chmod -R 755 $APP_CONF_VAR;
sudo chmod g+s $APP_CONFIG_CODEDIR;
sudo chmod g+s $APP_CONF_VAR;
"
fi

REMOTE_CMDS="
echo executing code type commands;
$codetype_cmds
echo completed execution of code type commands;
if [ ! -z \"${APP_APACHE_CONF_SSL}\" ] && [ \"$APP_SSL_EXISTS\" == \"yes\" ];
then
   echo creating $APACHE_CERT_DIR;
   sudo mkdir -p $APACHE_CERT_DIR;
   echo enable ssl;
   sudo a2enmod ssl;
   sudo a2dissite ${APP_APACHE_CONF_SSL};
   sudo a2ensite ${APP_APACHE_CONF_SSL}
fi;
sudo mkdir -p /var/log/apps;
sudo chmod -R 775  /var/log/apps;
sudo chgrp -R www-data /var/log/apps;
test \"\$conf_exists\" == \"yes\" && sudo service apache2 restart;
sudo chown -R $REMOTE_USER $APACHE_VHOST_CONF_DIR;
echo add web-admins if necessary;
getent group web-admins >/dev/null 2>&1 || sudo groupadd web-admins;
echo add svcjenkins to web-admins;
sudo usermod -a -G web-admins,www-data,tomcat8 svcjenkins;
echo create dirs if necessary;
sudo mkdir -p $APP_CONFIG_CODEDIR;
sudo mkdir -p $APP_CONF_VAR;
echo update permissions and ownership;
sudo chown -R $REMOTE_USER $APP_CONFIG_CODEDIR;
sudo chown -R $REMOTE_USER $APP_CONF_VAR;
sudo chgrp -R web-admins $APP_CONFIG_CODEDIR;
sudo chgrp -R web-admins $APP_CONF_VAR;
sudo chmod -R 775 $APP_CONFIG_CODEDIR;
sudo chmod -R 775 $APP_CONF_VAR;
"

echo "Current directory: $(pwd)"
echo "Writing remote commands to a file (deploy_codetypecmds_$$.sh) which will then be copied over to the destination server before execution"
echo "$REMOTE_CMDS" >deploy_codetypecmds_$$.sh
chmod 755 deploy_codetypecmds_$$.sh

echo "Now copying script to server"
rsync -Oravzhe "ssh -p $SSH_PORT" --delete --progress deploy_codetypecmds_$$.sh $REMOTE_USER@$APP_SERVER:/tmp/
if [ $? -ne 0 ]
then
   echo "ERROR: unable to copy script deploy_codetypecmds_$$.sh onto remote host: $REMOTE_USER@$APP_SERVER"
   exit 350
fi

ssh -t -t -o StrictHostKeyChecking=no -p $SSH_PORT $REMOTE_USER@$APP_SERVER  <<EOF
chmod 755 /tmp/deploy_codetypecmds_$$.sh;
sh -x /tmp/deploy_codetypecmds_$$.sh || exit 233;
exit
EOF
if [ $? -ne 0 ]
then
   echo "ERROR: unable to execute commands on remote host: $REMOTE_USER@$APP_SERVER"
   exit 301
fi

echo "copying code for $REPO_NAME to $APP_SERVER:$APP_CONFIG_CODEDIR (rsync)"
rsync -Oravzhe "ssh -p $SSH_PORT" --delete --progress $CODE_DIR/* $REMOTE_USER@$APP_SERVER:$APP_CONFIG_CODEDIR/
if [ $? -ne 0 ]
then
   echo "ERROR: unable to copy code for $REPO_NAME to $APP_SERVER:$APP_CONFIG_CODEDIR"
   exit 302
fi

if [ -d "$APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME" ]
then
   echo "env conf exists so creating config directory on server (if necessary) and then deploying config files"
      test -z "$(basename $APP_CONF_VAR | grep '\.')" &&  echo "create dir $APP_CONFIG_CODEDIR/config on $APP_SERVER"
   echo "logging onto $APP_SERVER to make config file/directory writable"
   ssh -t -t -o StrictHostKeyChecking=no -p $SSH_PORT $REMOTE_USER@$APP_SERVER <<EOF
   echo if config is a directory, then create it;
   test -z $(basename $APP_CONF_VAR | grep '\.') && sudo mkdir -p $APP_CONF_VAR;
   echo change permissions and ownership back to original settings;
   sudo chmod -R 775 $APP_CONF_VAR;
   sudo chown -R $REMOTE_USER $APP_CONF_VAR;
   exit
EOF
      
else
   echo "ERROR: no directory exists on git for $APP_CONFIG_ENVDIR/$REPO_NAME"
   exit 240
fi

if [ $( ls $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/${APP_APACHE_CONF_BASE}*.conf | wc -l ) -gt 0 ]
then
   if [ ! -z "$APP_CONF_VAR" ]
   then
      echo "copying code config directory [$APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/*] to $APP_SERVER:$APP_CONF_VAR/ (rsync)"
      rsync -Oravzhe "ssh -p $SSH_PORT" --delete --progress $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/* $REMOTE_USER@$APP_SERVER:$APP_CONF_VAR/
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to copy code config directory [$APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/${APP_APACHE_CONF}] to $APP_SERVER:$APP_CONF_VAR"
         exit 303
      fi
   else
      echo "WARNING: there is no specified config file/directory to deploy"
   fi
   if $( grep "VirtualHost" $APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/${APP_APACHE_CONF_BASE}*.conf >/dev/null 2>&1 )
   then
      echo "move code config(s) [$APP_CONF_VAR/${APP_APACHE_CONF_BASE}*.conf] to $APACHE_VHOST_CONF_DIR/ on $APP_SERVER"
      ssh -t -t -o StrictHostKeyChecking=no -p $SSH_PORT $REMOTE_USER@$APP_SERVER <<EOF
   echo move ssw cert files to $APACHE_CERT_DIR/.;
   sudo mv $APP_CONF_VAR/*.crt $APACHE_CERT_DIR/.;
   echo move ssw cert key files to $APACHE_CERT_DIR/.;
   sudo mv $APP_CONF_VAR/*.key $APACHE_CERT_DIR/.;
   echo move vhost conf files to $APACHE_VHOST_CONF_DIR/.;
   sudo mv $APP_CONF_VAR/${APP_APACHE_CONF_BASE}*.conf $APACHE_VHOST_CONF_DIR/.;
   exit \$?
EOF
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to copy code config(s) [$APP_CONFIG_REPO_DIR/$APP_CONFIG_ENVDIR/$REPO_NAME/${APP_APACHE_CONF_BASE}*.conf] to $APP_SERVER"
         exit 304
      fi
    fi
else
   echo "WARNING: no apache conf file(s) were found in the config directory [${APP_APACHE_CONF_BASE}*.conf]"
fi

if [ ! -z "$postcopy_cmds" ]
then
   echo "Executing post copy commands"
   ssh -t -t -o StrictHostKeyChecking=no -p $SSH_PORT $REMOTE_USER@$APP_SERVER <<EOF
$postcopy_cmds
conf_exists='no';
if [ ! -z "${APP_APACHE_CONF}" ] || [ ! -z "${APP_APACHE_CONF_SSL}" ];then 
   conf_exists='yes';
   sudo a2enmod rewrite || exit 800;
   
   if [ -f "$APACHE_VHOST_CONF_DIR/${APP_APACHE_CONF}" ];then
      echo add site ${APP_APACHE_CONF} to apache;
      sudo a2ensite ${APP_APACHE_CONF};
      test \$? -ne 0 && exit 801;
   fi;
   
   if [ -f "$APACHE_VHOST_CONF_DIR/${APP_APACHE_CONF_SSL}" ];then 
     echo add site ${APP_APACHE_CONF_SSL} to apache;
     sudo a2ensite ${APP_APACHE_CONF_SSL};
     test \$? -ne 0 && exit 802;
   fi;
   echo run apache configtest; 
   sudo apachectl configtest || exit 802;
   sudo grep 'Apache2 Ubuntu Default Page' /var/www/html/index.html && sudo mv /var/www/html/index.html /var/www/html/ubu_index.html.orig;
   if [ ! -f \"/var/www/html/index.html\" ];then
     sudo touch /var/www/html/index.html;
     sudo chmod 777 /var/www/html/index.html;
     echo 'contact sswits@uga.edu' >/var/www/html/index.html;
     sudo chmod 755 /var/www/html/index.html;
   fi;
     sudo chown -R root $APACHE_VHOST_CONF_DIR;
   
fi;
   if [ -f $APP_CONF_VAR/POST_DEPLOY_CMDS.sh ];then
      sudo chmod 755 $APP_CONF_VAR/POST_DEPLOY_CMDS.sh;
      sudo $APP_CONF_VAR/POST_DEPLOY_CMDS.sh;
      test \$? -ne 0 && exit 911;
   fi;

test "\$conf_exists" == 'yes' && sudo service apache2 restart;
exit \$?
EOF
   p_rc=$?
   if [ $p_rc -ne 0 ]
   then
      echo "ERROR: unable to execute postcopy commands on remote host: $REMOTE_USER@$APP_SERVER [rc:$p_rc]"
      exit $p_rc
   fi
fi

if [ ! -z "$APP_URL" ]
then
   echo "validating URL: $APP_URL"
   sleep 5
   curl -kIs $APP_URL
   curl_rc=$?
   if [ $curl_rc -ne 0 ]
   then
      echo "$APP_URL is not yet up, waiting 1 minute before checking again."
      sleep 60
      curl -kIs $APP_URL
      if [ $? -ne 0 ]
      then
         echo "ERROR: unable to access URL $APP_URL"
         exit 701
      else 
         echo "URL $APP_URL is up"
      fi
   else
      echo "URL $APP_URL is up"
   fi
fi
