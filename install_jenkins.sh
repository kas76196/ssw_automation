#!/bin/sh
#
#
# install jenkins
##########################################################
VERSION="1.0.4"
echo "starting jenkins install script [ version:  $VERSION ]"

wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
echo installing java
sudo apt-get -y install openjdk-8-jre-headless
echo installing jenkins
sudo apt-get -y install jenkins
echo installing python
sudo apt-get -y install python
echo installing expect
sudo apt-get -y install expect
echo installing connect-proxy so we can connect to internet with ssh
apt-get -y install connect-proxy
echo installing build-essential
sudo apt-get install -y build-essential
echo installing maven
apt install -y maven
echo php composer install/setup
sudo apt-get -y install curl php-cli php-mbstring git unzip
cd ~
sudo curl -sS https://getcomposer.org/installer -o composer-setup.php
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
sudo apt-get install -y php5.6-curl 
sudo apt-get install -y php5.6-xml


jenkins_home="/var/lib/jenkins"
if [ -d "$jenkins_home" ]
then
   mkdir -p $jenkins_home/.ssh
   echo  "
Host bitbucket.org
    Hostname bitbucket.org
    IdentityFile ~/.ssh/id_rsa
    IdentitiesOnly yes
    #ProxyCommand nc -X connect -x http://webav.uga.edu:8080 %h %p
    ProxyCommand connect -H http://webav.uga.edu:8080 %h %p
#Host *
#   ProxyCommand connect %h %p
" >$jenkins_home/.ssh/config
fi

echo "adding jenkins user to sudo"
sudo usermod -a -G sudo jenkins

if [ $? -eq 0 ]
then
   echo "find initial password file at /var/lib/jenkins/secrets/"
   echo "default service will run on port 8080 on this server"
fi


echo "Create /jenkins_backup directory if necessary"
sudo mkdir -p /jenkins_backup
sudo chown -R jenkins:jenkins /jenkins_backup

echo "Upgrade openssl"
cd /usr/src
openssl version | grep "1.0.2g"
if [ $? -eq 0 ]
then
    pwd
    wget https://www.openssl.org/source/openssl-1.0.2-latest.tar.gz
    tar -zxf openssl-1.0.2-latest.tar.gz 
    cd openssl-1.0.2?
    ./config
    make
    make test
    make install
    mv /usr/bin/openssl /root/
    ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl
fi
openssl version

echo update php ini file to enable extension=curl
sudo perl -p -i -e "s/^;extension=php_curl.dll/extension=php_curl.dll/" /etc/php/5.6/apache2/php.ini
sudo service apache2 restart

echo update iptables to route port 80 to 8080 and 443 to 8443
sudo iptables -A PREROUTING -t nat -i ens192 -p tcp --dport 80 -j REDIRECT --to-port 8080
sudo iptables -A PREROUTING -t nat -i ens192 -p tcp --dport 443 -j REDIRECT --to-port 8443
iptables -L -n
sleep 2
echo
echo execute the command below manually to persist iptables settings on reboot
echo sudo apt-get install iptables-persistent



