#!/bin/sh
#
# install setup
# This script should be executed after a server is provisioned.
# It performs the following tasks:
#   add uga internet proxy
#   updates postfix to relay to uga mail server
#   installs default packages
#   adds default users and setups up connectivity for jenkins
#   installs, enables sshd and activates port 22390 as default
#######################################################################
script_version="1.0.13"
jkusr="svcjenkins"
docker_images_dir="/docker_images"

################
# MAIN
################
echo $0 $script_version

internet_proxy_list="
http_proxy=\"http://webav.uga.edu:8080\"
https_proxy=\"http://webav.uga.edu:8080\"
HTTP_PROXY=\"http://webav.uga.edu:8080\"
HTTPS_PROXY=\"http://webav.uga.edu:8080\"
ftp_proxy=\"http://webav.uga.edu:8080\"
FTP_PROXY=\"http://webav.uga.edu:8080\"
no_proxy=\"localhost,127.0.0.1,::1\"
NO_PROXY=\"localhost,127.0.0.1,::1\"
"

for record in $internet_proxy_list
do
   grep "$record" /etc/environment
   if [ $? -ne 0 ]
   then
      echo "adding internet proxy record: $record"
      echo "$record" >>/etc/environment
   fi
done

echo "Source the profile to get env vars"
. $HOME/.profile

# GET UPDATES / INSTALL APPS
apt-get -y update
apt-get -y upgrade
apt-get -y install openssh-server
apt-get -y install docker-ce
apt-get -y install dos2unix
apt-get -y install jq
apt-get -y install default-jdk
apt-get -y install connect-proxy
#FOLLOWING MUST BE EXECUTED MANUALLY AS THEY FAIL WHEN AUTOMATED
#debconf-set-selections <<< "postfix postfix/mailname string $(hostname).ssw.uga.edu";
#debconf-set-selections <<< "postfix postfix/main_mailer_type string 'Internet Site'";
#apt-get -y install postfix
apt-get -y install mailutils

#Docker
mkdir -p $docker_images_dir
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-cache policy docker-ce
wget https://raw.githubusercontent.com/moby/moby/master/contrib/download-frozen-image-v2.sh
chmod 755 download-frozen-image-v2.sh



lsb_release -a
if [ -f "/etc/ssh/sshd_config" ]
then
   egrep "^Port 22$" /etc/ssh/sshd_config
   if [ $? -eq 0 ]
   then
      perl -pi.bak -e 's/Port 22/Port 22390/' /etc/ssh/sshd_config
      service sshd restart
      sleep 2
      service sshd status
   else
      echo "Port 22 is not in /etc/ssh/sshd_config"
      echo "Current port is set to: $(grep "^Port " /etc/ssh/sshd_config)"
   fi
else
   echo "ERROR: unable to find /etc/ssh/sshd_config"
fi

if [ -f "/etc/apt/apt.conf" ]
then
    grep 'http://webav.uga.edu:8080' /etc/apt/apt.conf
    if [ $? -ne 0 ]
    then
        cp /etc/apt/apt.conf /etc/apt/apt.conf.bak_$$
        echo "Acquire::http::Proxy \"http://webav.uga.edu:8080\";" >/etc/apt/apt.conf
    fi
fi


id $jkusr
if [ $? -ne 0 ]
then
   echo "Adding jenkins user $jkusr"
   adduser --quiet --disabled-password --gecos "Jenkins User" $jkusr
   usermod -aG sudo $jkusr
else
   echo "Jenkins user $jkusr already exists, skipping add..."
fi

groupadd web-admins
usermod -a -G web-admins $jkusr

id $jkusr
jkusr_exists=$?
if [ $jkusr_exists -eq 0 ]
then
    grep "$jkusr" /etc/sudoers
    if [ $? -ne 0 ]
    then 
        if ! $( grep $jkusr /etc/sudoers | grep -i NOPASSWD >/dev/null 2>&1 )
        then
           echo  "adding sudo permissions for $jkusr"
           echo  "$jkusr ALL=(ALL) NOPASSWD:ALL" >>/etc/sudoers
        fi
    fi

    jenkinskey="
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9kReNUsOO5/69Ma/+H1AISDOD7SFqmurPzPCLhQEFEHFRrBA0yqTcbKtkJ4tbkfA/mA+IupycwnS7K9DaI1rlkLTOda1aHQ5sqpU4HigLHh/D8cfA2E3AzXzX+uVBt3VvfUZBFjzoTzm0ZjMqc+rPoazV5U6zidW/II1s5PVjl2QIKohqig03zFJTglL6a8nMcsvZYBExTgwB3Sd+9ZCoQVQkt3Tn7YELC3HxtdzNkqs33GaJ7CwuDPU3rWL4dhc9GXQy7iauCb8S7NGIw+l0yfnfIXscFvgXEtPcfIKPHa0Kb3KPbMY2hwM0jUnjM6+ZCrbwmZKYJEEddSV0g0b7 jenkins@util1
    "

    grep "jenkins@util1" /home/$jkusr/.ssh/authorized_keys
    if [ $? -ne 0 ]
    then
        echo "adding ssh key for jenkins@util1"
        mkdir -p /home/$jkusr/.ssh
        echo "$jenkinskey" >>/home/$jkusr/.ssh/authorized_keys
    fi
else 
   echo "ERROR: jenkins users does not exist so sudo permissions and ssh key add not executed"
fi

jenkins_home="/var/lib/jenkins"
if [ -d "$jenkins_home" ]
then
   mkdir -p $jenkins_home/.ssh
   echo  "
Host bitbucket.org
    Hostname bitbucket.org
    IdentityFile ~/.ssh/id_rsa
    IdentitiesOnly yes
    #ProxyCommand nc -X connect -x http://webav.uga.edu:8080 %h %p
    ProxyCommand connect -H http://webav.uga.edu:8080 %h %p
#Host *
#   ProxyCommand connect %h %p
" >$jenkins_home/.ssh/config
fi

postfix_home="/etc/postfix"
if [ -d "$postfix_home" ]
then
   mailconfig="
# enable SASL authentication
smtp_sasl_auth_enable = yes
# disallow methods that allow anonymous authentication.
smtp_sasl_security_options = noanonymous
# where to find sasl_passwd
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
"
   echo "$mailconfig" >/tmp/mailcfg.txt
   if [ ! -f "$postfix_home/main.cf.orig" ]
   then 
      echo "create backup of main.cf"
      cp $postfix_home/main.cf $postfix_home/main.cf.orig
   fi

   if ! $( grep "relayhost" $postfix_home/main.cf | grep 'post.uga.edu' >/dev/null 2>&1 )
   then
      echo "update relayHost in main.cf"
      perl -pi -e 's/relayhost =/relayhost = post.uga.edu:587/;' $postfix_home/main.cf
   fi
   while read LINE
   do
      if ! $( grep "$LINE" $postfix_home/main.cf >/dev/null 2>&1 )
      then 
         echo "$LINE" >>$postfix_home/main.cf
      fi 
   done < /tmp/mailcfg.txt

   echo "configuring postfix"
   
   echo "generate sasl pass file"
   echo "post.uga.edu:587 s-sswmail:tucker1!" > /etc/postfix/sasl_passwd
   
   echo "execute postmap"
    postmap /etc/postfix/sasl_passwd
  echo "update permissions on postfix files"
   chown root:root /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
   chmod 0600 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
   echo "restart postfix"
   service postfix restart
fi

exit